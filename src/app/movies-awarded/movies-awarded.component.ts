import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {IResource} from '../core/models';

@Component({
  selector: 'app-movies-awarded',
  templateUrl: './movies-awarded.component.html',
  styleUrls: ['./movies-awarded.component.scss']
})
export class MoviesAwardedComponent implements OnInit {

  @Input() movies$: Observable<IResource[]>;

  constructor() { }

  ngOnInit() {
  }

}
