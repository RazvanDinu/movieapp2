import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoviesAwardedComponent } from './movies-awarded.component';

describe('MoviesAwardedComponent', () => {
  let component: MoviesAwardedComponent;
  let fixture: ComponentFixture<MoviesAwardedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoviesAwardedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesAwardedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
