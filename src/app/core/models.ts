export const RESOURCE_TYPES = {
  MOVIES: 'movies',
};

export type GenreType = 'action' | 'adventure' | 'biography' | 'comedy' | 'crime'
  | 'drama' | 'history' | 'mystery' | 'scifi' | 'sport' | 'thriller';

export const genreType = {
  action: 'action' as GenreType,
  adventure: 'adventure' as GenreType,
  biography: 'biography' as GenreType,
  comedy: 'comedy' as GenreType,
  crime: 'crime' as GenreType,
  drama: 'drama' as GenreType,
  history: 'history' as GenreType,
  mystery: 'mystery' as GenreType,
  scifi: 'scifi' as GenreType,
  sport: 'sport' as GenreType,
  thriller: 'thriller' as GenreType
};

export type ResourceType = string;

export interface IResource {
  id: string;
  slug: string;
  name: string;
  description: string;
  genres: GenreType[];
  rate: string;
  length: string;
  img: string;
  year: string;
}

export interface IResourceList {
  items: {};
  loading: boolean;
  error: any;
}

export const fromServer = (record: any): IResource => ({
  id: record.name.toLowerCase(),
  slug: record.slug,
  name: record.name,
  description: record.description,
  genres: record.genres,
  rate: record.rate,
  length: record.length,
  img: record.img,
  year: record.year
});
