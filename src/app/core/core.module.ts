import {NgModule, Optional, SkipSelf} from '@angular/core';
import { CommonModule } from '@angular/common';
import {ResourceAPIEpics} from './api/resource-epic';
import {ResourceAPIService} from './service';
import {ResourceAPIActions} from './api/resource-actions';
import {throwIfAlreadyLoaded} from './module-import-guard';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
    ResourceAPIActions,
    ResourceAPIEpics,
    ResourceAPIService]
})
export class CoreModule {
  constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}
