import { indexBy, prop } from 'ramda';
import { Action } from 'redux';
import {IResourceList, ResourceType} from '../models';
import {ResourceAPIAction, ResourceAPIActions} from './resource-actions';

const INITIAL_STATE: IResourceList = {
  items: {},
  loading: false,
  error: null,
};


export function createResourceAPIReducer(resourceType: ResourceType) {
  return function animalReducer(
      state: IResourceList = INITIAL_STATE,
      a: Action): IResourceList
  {

    const action = a as ResourceAPIAction;
    if (!action.meta || action.meta.resourceType !== resourceType) {
      return state;
    }

    switch (action.type) {
      case ResourceAPIActions.LOAD_STARTED:
        return {
          ...state,
          items: {},
          loading: true,
          error: null,
        };
      case ResourceAPIActions.LOAD_SUCCEEDED:
        return {
          ...state,
          items: indexBy(prop('id'), action.payload),
          loading: false,
          error: null,
        };
      case ResourceAPIActions.LOAD_FAILED:
        return {
          ...state,
          items: {},
          loading: false,
          error: action.error,
        };
    }

    return state;
  };
}
