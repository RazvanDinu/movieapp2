import { Injectable } from '@angular/core';
import { dispatch } from '@angular-redux/store';
import { FluxStandardAction } from 'flux-standard-action';
import {IResource, ResourceType} from '../models';

type Payload = IResource[];
interface MetaData { resourceType: ResourceType; };
export type ResourceAPIAction = FluxStandardAction<Payload, MetaData>;

@Injectable()
export class ResourceAPIActions {
  static readonly LOAD_RESOURCE = 'LOAD_RESOURCE';
  static readonly LOAD_STARTED = 'LOAD_STARTED';
  static readonly LOAD_SUCCEEDED = 'LOAD_SUCCEEDED';
  static readonly LOAD_FAILED = 'LOAD_FAILED';

  @dispatch()
  loadResources = (resourceType: ResourceType): ResourceAPIAction => ({
    type: ResourceAPIActions.LOAD_RESOURCE,
    meta: { resourceType },
    payload: null,
  })

  loadStarted = (resourceType: ResourceType): ResourceAPIAction => ({
    type: ResourceAPIActions.LOAD_STARTED,
    meta: { resourceType },
    payload: null,
  })

  loadSucceeded = (resourceType: ResourceType, payload: Payload): ResourceAPIAction => ({
    type: ResourceAPIActions.LOAD_SUCCEEDED,
    meta: { resourceType },
    payload,
  })

  loadFailed = (resourceType: ResourceType, error): ResourceAPIAction => ({
    type: ResourceAPIActions.LOAD_FAILED,
    meta: { resourceType },
    payload: null,
    error,
  })
}
