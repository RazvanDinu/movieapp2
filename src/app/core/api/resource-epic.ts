import {Injectable} from '@angular/core';
import {ResourceAPIService} from '../service';
import {ResourceType} from '../models';
import {IAppState} from '../../store/model';
import {createEpicMiddleware, Epic} from 'redux-observable';
import {ResourceAPIAction, ResourceAPIActions} from './resource-actions';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/startWith';

const resourceNotAlreadyFetched = (
  resourceType: ResourceType,
  state: IAppState): boolean => !(
  state[resourceType] &&
  state[resourceType].items &&
  Object.keys(state[resourceType].items).length);

const actionIsForCorrectResourceType = (resourceType: ResourceType) =>
  (action: ResourceAPIAction): boolean =>
  action.meta.resourceType === resourceType;

@Injectable()
export class ResourceAPIEpics {
  constructor(
    private service: ResourceAPIService,
    private actions: ResourceAPIActions,
  ) {}

  public createEpic(resourceType: ResourceType) {
    return createEpicMiddleware(this.createLoadResourceEpic(resourceType));
  }

  private createLoadResourceEpic(resourceType: ResourceType): Epic<ResourceAPIAction, IAppState> {
    return (action$, store) => action$
      .ofType(ResourceAPIActions.LOAD_RESOURCE)
      .filter(action => actionIsForCorrectResourceType(resourceType)(action))
      .filter(() => resourceNotAlreadyFetched(resourceType, store.getState()))
      .switchMap(() => this.service.getAll(resourceType)
        .map(data => this.actions.loadSucceeded(resourceType, data))
        .catch(response => of(this.actions.loadFailed(resourceType, {
          status: '' + response.status,
        })))
        .startWith(this.actions.loadStarted(resourceType)));
  }
}
