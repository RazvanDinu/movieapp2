import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import {fromServer, IResource, RESOURCE_TYPES, ResourceType} from './models';

const URLS = {
  [RESOURCE_TYPES.MOVIES]: '/api/movies',
};

@Injectable()
export class ResourceAPIService {
  constructor(private http: Http) {}

  getAll = (resourceType: ResourceType): Observable<IResource[]> =>
    this.http.get(URLS[resourceType])
      .map(resp => resp.json())
      .map(records => records.map(fromServer))
}
