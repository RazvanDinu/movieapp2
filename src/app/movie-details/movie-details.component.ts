import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import {select, select$} from '@angular-redux/store';
import {Observable} from 'rxjs/Observable';
import { pipe, values, sortBy, prop } from 'ramda';
import {IResource, RESOURCE_TYPES} from '../core/models';
import {ResourceAPIActions} from '../core/api/resource-actions';
import {ActivatedRoute} from '@angular/router';

export const sortMovies = (animalDictionary$: Observable<{}>) =>
  animalDictionary$.map(
    pipe(
      values,
      sortBy(prop('name'))));

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit {

  @select$(['movies', 'items'], sortMovies)
  readonly movies$: Observable<IResource[]>;

  @select(['movies', 'loading'])
  readonly loading$: Observable<boolean>;

  @select(['movies', 'error'])
  readonly error$: Observable<any>;

  slug: string;

  constructor(
    protected actions: ResourceAPIActions,
    protected location: Location,
    protected route: ActivatedRoute,
  ) {
    this.slug = this.route.snapshot.params['slug'];
    this.actions.loadResources(RESOURCE_TYPES.MOVIES);
  }

  ngOnInit() {}



  goBack() {
    this.location.back();
  }

}
