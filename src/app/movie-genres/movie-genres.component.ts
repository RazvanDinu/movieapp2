import {Component, Input, OnInit} from '@angular/core';
import {GenreType} from '../core/models';

export interface Filter {
  searchTerm: string;
  genre: GenreType|string;
}

@Component({
  selector: 'app-movie-genres',
  templateUrl: './movie-genres.component.html',
  styleUrls: ['./movie-genres.component.scss']
})
export class MovieGenresComponent implements OnInit {

  @Input() genres: GenreType[];
  @Input() filters: Filter;

  constructor() { }

  ngOnInit() {
  }

}
