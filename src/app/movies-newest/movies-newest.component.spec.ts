import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoviesNewestComponent } from './movies-newest.component';

describe('MoviesNewestComponent', () => {
  let component: MoviesNewestComponent;
  let fixture: ComponentFixture<MoviesNewestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoviesNewestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesNewestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
