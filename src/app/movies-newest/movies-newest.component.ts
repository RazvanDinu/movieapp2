import {Component, Input, OnInit} from '@angular/core';

import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {IResource} from '../core/models';

@Component({
  selector: 'app-movies-newest',
  templateUrl: './movies-newest.component.html',
  styleUrls: ['./movies-newest.component.scss']
})
export class MoviesNewestComponent implements OnInit {

  @Input() movies$: Observable<IResource[]>;

  constructor(
    protected router: Router
  ) {

  }

  ngOnInit() { }

  showDetail(slug) {
    console.log(slug);
    this.router.navigate(['movies', slug]);
  }

}
