import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoviesComponent } from './movies.component';
import {SharedModule} from '../shared/shared.module';
import {MoviesRoutingModule} from './movies-routing.module';
import {MoviesAwardedComponent} from '../movies-awarded/movies-awarded.component';
import {CarouselComponent} from '../carousel/carousel.component';
import {MovieDetailsComponent} from '../movie-details/movie-details.component';
import {MoviesListComponent} from '../movies-list/movies-list.component';
import {MovieGenresComponent} from '../movie-genres/movie-genres.component';
import {MoviesNewestComponent} from '../movies-newest/movies-newest.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MoviesRoutingModule,
    FormsModule,
    RouterModule,
  ],
  declarations: [
    MoviesComponent,
    CarouselComponent,
    MovieDetailsComponent,
    MoviesListComponent,
    MovieGenresComponent,
    MoviesNewestComponent,
    MoviesAwardedComponent,
  ],
  exports: [MoviesComponent]
})
export class MoviesModule { }
