import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ResourceAPIActions} from '../core/api/resource-actions';
import {NgRedux, select, select$} from '@angular-redux/store';
import {Observable} from 'rxjs/Observable';
import { pipe, values, sortBy, prop } from 'ramda';

import {IResource, RESOURCE_TYPES} from '../core/models';
import {IAppState} from '../store/model';

export const sortMovies = (animalDictionary$: Observable<{}>) =>
  animalDictionary$.map(
    pipe(
      values,
      sortBy(prop('name'))));

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MoviesComponent implements OnInit {

  @select$(['movies', 'items'], sortMovies)
  readonly movies$: Observable<IResource[]>;

  @select(['movies', 'loading'])
  readonly loading$: Observable<boolean>;

  @select(['movies', 'error'])
  readonly error$: Observable<any>;

  constructor(
    protected actions: ResourceAPIActions,
    protected ngRedux: NgRedux<IAppState>
  ) {
    this.actions.loadResources(RESOURCE_TYPES.MOVIES);
  }

  ngOnInit() {
    // this.ngRedux.select('movies').subscribe(data => console.log(data));
  }

}
