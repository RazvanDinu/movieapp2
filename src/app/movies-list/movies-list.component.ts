import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {select, select$} from '@angular-redux/store';
import 'rxjs/add/operator/map';
import { pipe, values, sortBy, prop } from 'ramda';

import {GenreType, genreType, IResource, RESOURCE_TYPES} from '../core/models';
import {ResourceAPIActions} from '../core/api/resource-actions';
import {Observable} from 'rxjs/Observable';

export interface Filter {
  searchTerm: string;
  genre: GenreType|string;
}

export const sortMovies = (animalDictionary$: Observable<{}>) =>
  animalDictionary$.map(
    pipe(
      values,
      sortBy(prop('name'))));

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss']
})
export class MoviesListComponent implements OnInit {

  @select$(['movies', 'items'], sortMovies)
  readonly movies$: Observable<IResource[]>;

  @select(['movies', 'loading'])
  readonly loading$: Observable<boolean>;

  @select(['movies', 'error'])
  readonly error$: Observable<any>;

  listClassType: string;
  genreTypes: GenreType[];
  filters: Filter;
  currentPage: number;

  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    protected actions: ResourceAPIActions,
  ) {
    this.actions.loadResources(RESOURCE_TYPES.MOVIES);
    this.listClassType = 'list-group-item';
    this.genreTypes = [
      genreType.action,
      genreType.adventure,
      genreType.biography,
      genreType.comedy,
      genreType.crime,
      genreType.drama,
      genreType.history,
      genreType.mystery,
      genreType.thriller,
      genreType.scifi,
      genreType.sport
    ];
    this.filters = {
      searchTerm: '',
      genre: ''
    };
    this.currentPage = 1;
  }

  ngOnInit() {
    this.route
      .paramMap
      .map(paramsMap => paramsMap).subscribe(paramsMap => {
        this.filters.searchTerm = paramsMap.get('search') || '';
        this.filters.genre = paramsMap.get('genre') || '';
        this.currentPage = 1;
    });
  }

  viewMovieDetails(slug) {
    this.router.navigate(['movies', slug]);
  }

  changeListView(viewName) {
    this.listClassType = viewName;
  }

  updateFilters() {
    this.router.navigate(['movies', this.filters]);
  }

  loadMore() {
    if (this.currentPage > 0) {
      this.currentPage++;
    }
  }

}
