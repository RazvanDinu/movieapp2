import {Injectable} from '@angular/core';
import {ResourceAPIEpics} from '../core/api/resource-epic';
import {RESOURCE_TYPES} from '../core/models';

@Injectable()
export class RootEpics {
  constructor(private resourceEpics: ResourceAPIEpics) {}

  public createEpics() {
    return [
      this.resourceEpics.createEpic(RESOURCE_TYPES.MOVIES),
    ];
  }
}
