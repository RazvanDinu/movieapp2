import {composeReducers, defaultFormReducer} from '@angular-redux/form';
import {combineReducers} from 'redux';
import {routerReducer} from '@angular-redux/router';
import {createResourceAPIReducer} from '../core/api/resource-reducer';
import {RESOURCE_TYPES} from '../core/models';

export const rootReducer = composeReducers(
  defaultFormReducer(),
  combineReducers({
    router: routerReducer,
    movies: createResourceAPIReducer(RESOURCE_TYPES.MOVIES)
  })
);
