import {IResourceList} from '../core/models';

export interface IAppState {
  [animalType: string]: IResourceList;
  routes?: any;
  feedback?: any;
}
