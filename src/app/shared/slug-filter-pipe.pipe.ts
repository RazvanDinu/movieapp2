import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'slugFilterPipe'
})
export class SlugFilterPipePipe implements PipeTransform {

  transform(items: Array<any>, slug: string): Array<any> {
    return items.filter(item => item.slug === slug);
  }

}
