import { Pipe, PipeTransform } from '@angular/core';
import {GenreType} from '../core/models';

export interface Filter {
  searchTerm: string;
  genre: GenreType|string;
}

@Pipe({
  name: 'searchFilterPipe',
  pure: false
})
export class SearchFilterPipePipe implements PipeTransform {

  transform(value: any, filters: Filter, field: string): any {
    return filters.searchTerm || filters.genre ? value.reduce((prev, next) => {
      if (filters.searchTerm && filters.searchTerm !== '' && !filters.genre) {
        console.log('search');
        if (next[field].toLowerCase().includes(filters.searchTerm)) { prev.push(next); }
      } else if (filters.genre && filters.genre !== '' && !filters.searchTerm && filters.searchTerm == '') {
        console.log('genre');
        next['genres'].forEach(function (val) {
          if (val === filters.genre) {
            prev.push(next);
          }
        });
      } else {
        console.log('both');
        if (next[field].toLowerCase().includes(filters.searchTerm)) {
          next['genres'].forEach(function (val) {
            if (val === filters.genre) {
              prev.push(next);
            }
          });
        }
      }

      return prev;
    }, []) : value;
  }

}
