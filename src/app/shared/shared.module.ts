import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LoadingComponent} from '../loading/loading.component';
import {ErrorComponent} from '../error/error.component';
import {SetBackgroundDirective} from './set-background.directive';
import {FooterComponent} from '../footer/footer.component';
import {PageNotFoundComponent} from '../page-not-found/page-not-found.component';
import { SlugFilterPipePipe } from './slug-filter-pipe.pipe';
import { SearchFilterPipePipe } from './search-filter-pipe.pipe';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    LoadingComponent,
    ErrorComponent,
    SetBackgroundDirective,
    FooterComponent,
    PageNotFoundComponent,
    SlugFilterPipePipe,
    SearchFilterPipePipe,
  ],
  exports: [
    LoadingComponent,
    ErrorComponent,
    SetBackgroundDirective,
    FooterComponent,
    PageNotFoundComponent,
    SlugFilterPipePipe,
    SearchFilterPipePipe,
  ]
})
export class SharedModule { }
