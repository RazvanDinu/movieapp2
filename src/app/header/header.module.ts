import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';

import {HeaderComponent} from './header.component';
import {HeaderNavComponent} from '../header-nav/header-nav.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
  ],
  declarations: [
    HeaderComponent,
    HeaderNavComponent,
  ],
  exports: [
    HeaderComponent
  ]
})
export class HeaderModule { }
